module go.elara.ws/drpc

go 1.18

require (
	github.com/hashicorp/yamux v0.1.1
	storj.io/drpc v0.0.32
)

require github.com/zeebo/errs v1.2.2 // indirect
